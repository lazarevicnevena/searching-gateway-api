package rs.ac.uns.ftn.siit.searchinggateway.service;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;


import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

@SuppressWarnings("ALL")
@Service
public class FileService {

    private static String DATA_DIRECTORY_PATH;

    static {
        ResourceBundle rb=ResourceBundle.getBundle("application");
        DATA_DIRECTORY_PATH=rb.getString("dataDirectory");
    }


    public File getFilePath(String path) {
        Class k = this.getClass();
        ClassLoader l = k.getClassLoader();
        URL url = this.getClass().getClassLoader().getResource(path);
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        return file;
    }

    public String saveFile(MultipartFile file, String authorEmail, String title) throws IOException {
        String retVal = null;
        if (! file.isEmpty()) {
            byte[] bytes = file.getBytes();
            String location = getFilePath(DATA_DIRECTORY_PATH).getAbsolutePath() + File.separator +
                    authorEmail;
            new File(location).mkdirs();
            String fullLocation = location + File.separator + title + ".pdf";
            Path path = Paths.get(fullLocation);
            Files.write(path, bytes);
            retVal = path.toString();
        }
        return retVal;
    }

    public String saveFile(MultipartFile file) throws IOException {
        String retVal = null;
        if (! file.isEmpty()) {
            byte[] bytes = file.getBytes();
            String [] tokens = file.getOriginalFilename().split("\\\\");
            String name = tokens[tokens.length-1];
            String location = getFilePath(DATA_DIRECTORY_PATH).getAbsolutePath() + File.separator + "search" + File.separator + name;
            Path path = Paths.get(location);
            Files.write(path, bytes);
            retVal = path.toString();
        }
        return retVal;
    }

    public MultipartFile convertToMultiPart(File file){
        DiskFileItem fileItem =
                new DiskFileItem("file", "text/plain",
                        false, file.getName(), (int) file.length() , file.getParentFile());
        try {
            fileItem.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
        return multipartFile;
    }
}

package rs.ac.uns.ftn.siit.searchinggateway.handler;

import org.apache.lucene.document.DateTools;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.text.PDFTextStripper;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.siit.searchinggateway.model.*;
import rs.ac.uns.ftn.siit.searchinggateway.service.FileService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

public class PDFHandler {

    public IndexingUnit getIndexingUnit(UploadModel uploadModel, File file) {
        IndexingUnit indexingUnit = new IndexingUnit();
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            PDFParser parser = new PDFParser(randomAccessFile);
            parser.parse();
            String text = getText(parser);
            indexingUnit.setContent(text);

            randomAccessFile.close();

            MetaDataInfo metaDataInfo = uploadModel.getMetaDataInfo();

            String title = metaDataInfo.getTitle();
            indexingUnit.setTitle(title);

            String journal = metaDataInfo.getJournalTitle();
            indexingUnit.setJournal(journal);

            String subjectArea = metaDataInfo.getSubjectArea();
            indexingUnit.setSubjectArea(subjectArea);

            String abstractText = metaDataInfo.getShortAbstract();
            indexingUnit.setAbstract_(abstractText);

            String doi = metaDataInfo.getDoi();
            indexingUnit.setDoi(doi);

            Boolean openAccess = metaDataInfo.getOpenAccess();
            indexingUnit.setOpenAccess(openAccess);

            ArrayList<Author> authors = new ArrayList<>();
            for (Author a: metaDataInfo.getAuthors()){
                try{
                    a.setCoordinates(getCoordinates(a.getCity(), a.getState()));
                }catch (Exception e){
                    System.out.println("Nisam uspjela vratiti koordinate");
                }
                authors.add(a);
            }

            indexingUnit.setAuthors(authors);

            ArrayList<Reviewer> reviewers = new ArrayList<>();
            for (Reviewer r: uploadModel.getReviewers()){
                try{
                    r.setCoordinates(getCoordinates(r.getCity(), r.getState()));
                }catch (Exception e){
                    System.out.println("Nisam uspjela vratiti koordinate");
                }
                reviewers.add(r);
            }

            indexingUnit.setReviewers(reviewers);


            String keywords = metaDataInfo.getKeywords();
            if(keywords != null){
                String[] splittedKeywords = keywords.split(",");
                indexingUnit.setKeywords(new ArrayList<>(Arrays.asList(splittedKeywords)));
            }

            indexingUnit.setFilename(file.getCanonicalPath());

            String modificationDate= DateTools.dateToString(new Date(file.lastModified()),DateTools.Resolution.DAY);
            indexingUnit.setFiledate(modificationDate);

        } catch (IOException e) {
            System.out.println("Greska pri konvertovanju dokumenta u pdf");
        }

        return indexingUnit;
    }



    public String getText(PDFParser parser) {
        try {
            PDFTextStripper textStripper = new PDFTextStripper();
            textStripper.setStartPage(2);
            String text = textStripper.getText(parser.getPDDocument());
            return text;
        } catch (IOException e) {
            System.out.println("Greksa pri konvertovanju dokumenta u pdf");
        }
        return null;
    }

    public GeoPoint getCoordinates(String city, String state) {
        Map<String, Double> coords;

        coords = OpenStreetMapUtils.getInstance().getCoordinates(city + ", " + state);

        GeoPoint coordinates = new GeoPoint(coords.get("lat"), coords.get("lon"));

        return coordinates;
    }

}

package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;
import rs.ac.uns.ftn.siit.searchinggateway.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.searchinggateway.dto.MetaDataInfoDTO;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MetaDataInfo {

    private String title;
    private String journalTitle;
    private String keywords;
    private String shortAbstract;
    private String subjectArea;
    private String doi;
    private Boolean openAccess;

    private List<Author> authors = new ArrayList<>();

    public MetaDataInfo(MetaDataInfoDTO metaDataInfoDTO){
        this.title = metaDataInfoDTO.getTitle();
        this.journalTitle = metaDataInfoDTO.getJournalTitle();
        this.keywords = metaDataInfoDTO.getKeywords();
        this.shortAbstract = metaDataInfoDTO.getShortAbstract();
        this.subjectArea = metaDataInfoDTO.getSubjectArea();
        this.doi = metaDataInfoDTO.getDoi();
        this.openAccess = metaDataInfoDTO.getOpenAccess();
        for (AuthorDTO a: metaDataInfoDTO.getAuthors()){
            this.authors.add(new Author(a));
        }
    }


}

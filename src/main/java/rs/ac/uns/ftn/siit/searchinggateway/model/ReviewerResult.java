package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReviewerResult {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;
    private String rank;


}

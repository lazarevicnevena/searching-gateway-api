package rs.ac.uns.ftn.siit.searchinggateway.service;

import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.searchinggateway.handler.ResultRetriever;
import rs.ac.uns.ftn.siit.searchinggateway.model.ResultData;
import rs.ac.uns.ftn.siit.searchinggateway.model.ResultHighlighter;
import rs.ac.uns.ftn.siit.searchinggateway.model.Reviewer;
import rs.ac.uns.ftn.siit.searchinggateway.model.ReviewerResult;

import java.util.List;

@Service
public class SearchingService {

    public List<ResultData> getResultData(QueryBuilder builder, List<ResultHighlighter> resultHighlighters) {
        return ResultRetriever.getResults(builder, resultHighlighters);
    }

    public List<ReviewerResult> getReviewerResultData(QueryBuilder queryBuilder) {
        return ResultRetriever.getReviewerResultData(queryBuilder);
    }

    public List<ReviewerResult> getReviewerResultDataMoreLikeThis(QueryBuilder queryBuilder) {
        return ResultRetriever.getReviewerResultDataMoreLikeThis(queryBuilder);
    }
}

package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import rs.ac.uns.ftn.siit.searchinggateway.dto.AuthorDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Author {

    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;

    @GeoPointField
    private GeoPoint coordinates;

    public Author(AuthorDTO authorDTO){
        this.firstName = authorDTO.getFirstName();
        this.lastName = authorDTO.getLastName();
        this.email = authorDTO.getEmail();
        this.city = authorDTO.getCity();
        this.state = authorDTO.getState();
        this.coordinates = new GeoPoint();
    }

}



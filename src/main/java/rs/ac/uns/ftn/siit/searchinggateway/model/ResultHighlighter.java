package rs.ac.uns.ftn.siit.searchinggateway.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public final class ResultHighlighter {
	
	private String field;
	private String value;

}

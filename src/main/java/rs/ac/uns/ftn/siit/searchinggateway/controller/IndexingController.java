package rs.ac.uns.ftn.siit.searchinggateway.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.elasticsearch.common.geo.GeoPoint;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import rs.ac.uns.ftn.siit.searchinggateway.dto.MetaDataInfoDTO;
import rs.ac.uns.ftn.siit.searchinggateway.dto.ReviewerDTO;
import rs.ac.uns.ftn.siit.searchinggateway.dto.UploadModelDTO;
import rs.ac.uns.ftn.siit.searchinggateway.model.*;
import rs.ac.uns.ftn.siit.searchinggateway.service.FileService;
import rs.ac.uns.ftn.siit.searchinggateway.service.IndexingService;

import javax.servlet.annotation.MultipartConfig;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

@RestController
@RequestMapping( value = "/indexes" )
public class IndexingController {

    @Autowired
    IndexingService indexingService;

    @Autowired
    FileService fileService;

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<String> multiUploadFileModel(@RequestParam("file") MultipartFile file,
                                                       @RequestParam("metaDataInfo")String metaDataInfoDTOString,
                                                       @RequestParam("reviewers") String reviewersString) throws IOException, ParseException {

        UploadModel uploadModel = new UploadModel();
        uploadModel.setFile(file);

        Gson gson=new GsonBuilder().create();

        MetaDataInfoDTO metaDataInfoDTO =gson.fromJson(metaDataInfoDTOString,MetaDataInfoDTO.class);

        ObjectMapper mapper = new ObjectMapper();
        List<ReviewerDTO> reviewers = mapper.readValue(reviewersString,  new TypeReference<List<ReviewerDTO>>(){});

        uploadModel.setMetaDataInfo(new MetaDataInfo(metaDataInfoDTO));
        for (ReviewerDTO reviewerDTO: reviewers){
            uploadModel.getReviewers().add(new Reviewer(reviewerDTO));
        }

        System.out.println("Uploading file!");
        try {
            indexingService.indexFile(uploadModel);

        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("File successfully uploaded and indexed!", HttpStatus.OK);

    }


    @RequestMapping(
            value = "/scientific-center",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> setAnalyzer(@RequestBody IndexConfiguration indexConfiguration) throws IOException {

        Boolean success = indexingService.setScientificCenterAnalyzer(indexConfiguration);

        if (!success) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Analyzer successfully set!", HttpStatus.OK);

    }

    @RequestMapping(
            value = "/reviewers",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> setReviewersAnalyzer(@RequestBody IndexConfiguration indexConfiguration) throws IOException {

        Boolean success = indexingService.setReviewersAnalyzer(indexConfiguration);

        if (!success) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Analyzer successfully set!", HttpStatus.OK);

    }



}

package rs.ac.uns.ftn.siit.searchinggateway.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MetaDataInfoDTO {

    private String title;
    private String journalTitle;
    private String keywords;
    private String shortAbstract;
    private String subjectArea;
    private String doi;
    private Boolean openAccess;

    private List<AuthorDTO> authors = new ArrayList<>();

}

package rs.ac.uns.ftn.siit.searchinggateway.model;

import io.searchbox.annotations.JestId;
import lombok.*;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import rs.ac.uns.ftn.siit.searchinggateway.dto.ReviewerDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Reviewer {

    @JestId
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;
    private String rank;

    @GeoPointField
    private GeoPoint coordinates;

    public Reviewer(ReviewerDTO reviewerDTO){
        this.username = reviewerDTO.getUsername();
        this.firstName = reviewerDTO.getFirstName();
        this.lastName = reviewerDTO.getLastName();
        this.email = reviewerDTO.getEmail();
        this.city = reviewerDTO.getCity();
        this.state = reviewerDTO.getState();
        this.rank = reviewerDTO.getRank();
        this.coordinates = new GeoPoint();
    }

}

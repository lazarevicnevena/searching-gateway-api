package rs.ac.uns.ftn.siit.searchinggateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchingGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchingGatewayApplication.class, args);
	}

}


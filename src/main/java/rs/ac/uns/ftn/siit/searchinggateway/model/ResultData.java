package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ResultData {

    private String title;
    private String journal;
    private String subjectArea;
    private String keywords;
    private String location;
    private Boolean openAccess;
    private String doi;
    private String authors;
    private String highlight;

}

package rs.ac.uns.ftn.siit.searchinggateway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.searchinggateway.indexing.Indexer;
import rs.ac.uns.ftn.siit.searchinggateway.model.IndexConfiguration;
import rs.ac.uns.ftn.siit.searchinggateway.model.IndexingUnit;
import rs.ac.uns.ftn.siit.searchinggateway.model.Reviewer;
import rs.ac.uns.ftn.siit.searchinggateway.model.UploadModel;

import java.io.File;
import java.io.IOException;

@Service
public class IndexingService {

    @Autowired
    FileService fileService;

    public void indexFile(UploadModel uploadModel) throws IOException {
        if (uploadModel.getFile().isEmpty()) {
            return;
        }

        String fileName = fileService.saveFile(uploadModel.getFile(),
                uploadModel.getMetaDataInfo().getAuthors().get(0).getEmail(),
                uploadModel.getMetaDataInfo().getTitle());

        System.out.println("FILE NAME: " + fileName);

        if(fileName != null){
            IndexingUnit indexUnit = Indexer.getInstance().getHandler().getIndexingUnit(
                    uploadModel, new File(fileName));
            Boolean result = Indexer.getInstance().addIndexingUnit(indexUnit);

            System.out.println("SW IS UPLOADED: " + result);
            if (result){
                if (!uploadModel.getReviewers().isEmpty()){
                    for (Reviewer reviewer: uploadModel.getReviewers()){
                        Indexer.getInstance().addReviewerIndexingUnit(reviewer);
                    }
                }
            }
        }
    }

    public boolean setScientificCenterAnalyzer(IndexConfiguration indexConfiguration) throws IOException {

        return Indexer.getInstance().setScientificCenterAnalyzer(
                indexConfiguration.getSettings().toString(),
                indexConfiguration.getMappings().toString());

    }

    public boolean setReviewersAnalyzer(IndexConfiguration indexConfiguration) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        String settings = mapper.writeValueAsString(indexConfiguration.getSettings());
        String mappings = mapper.writeValueAsString(indexConfiguration.getMappings());

        return Indexer.getInstance().setReviewersAnalyzer(settings, mappings);

    }

}

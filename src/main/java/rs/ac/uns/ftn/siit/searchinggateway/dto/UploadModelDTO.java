package rs.ac.uns.ftn.siit.searchinggateway.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UploadModelDTO {

    private MetaDataInfoDTO metaDataInfo;

    private MultipartFile file;

    private List<ReviewerDTO> reviewers = new ArrayList<>();
}

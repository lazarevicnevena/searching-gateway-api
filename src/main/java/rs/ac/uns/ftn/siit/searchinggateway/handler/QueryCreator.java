package rs.ac.uns.ftn.siit.searchinggateway.handler;


import lombok.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.elasticsearch.index.query.QueryBuilder;

import org.elasticsearch.index.query.QueryBuilders;
import rs.ac.uns.ftn.siit.searchinggateway.model.QueryType;

@Getter
@Setter
@ToString
public class QueryCreator {
	
	private static int maxEdits = 1;

	
	public static QueryBuilder buildQuery(QueryType queryType, String field, String value) throws IllegalArgumentException, ParseException{

	    Boolean flag = false;

	    if(field == null || field.equals("")){
			flag = true;
		}
		if(value == null){
			flag = true;
		}
		if(flag){
			throw new IllegalArgumentException("Field id or field value not specified!");
		}
		
		QueryBuilder queryBuilder = null;
		if(queryType.equals(QueryType.REGULAR)){
			queryBuilder = QueryBuilders.matchQuery(field, value);
		} else if (queryType.equals(QueryType.PHRASE)){
            queryBuilder = QueryBuilders.matchPhraseQuery(field, value);
        }

		return queryBuilder;
	}


}

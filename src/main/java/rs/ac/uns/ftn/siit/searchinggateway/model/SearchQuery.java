package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SearchQuery {

    private String field;
    private String value;

    // AND || OR
    private String operator;

    // regular || phrase
    private String type;

}

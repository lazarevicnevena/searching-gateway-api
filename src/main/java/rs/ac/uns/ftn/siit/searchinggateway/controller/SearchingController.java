package rs.ac.uns.ftn.siit.searchinggateway.controller;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.elasticsearch.common.lucene.search.MoreLikeThisQuery;
import org.elasticsearch.index.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.siit.searchinggateway.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.searchinggateway.handler.QueryCreator;
import rs.ac.uns.ftn.siit.searchinggateway.indexing.Indexer;
import rs.ac.uns.ftn.siit.searchinggateway.model.*;
import rs.ac.uns.ftn.siit.searchinggateway.service.FileService;
import rs.ac.uns.ftn.siit.searchinggateway.service.SearchingService;
import org.elasticsearch.common.geo.GeoPoint;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping( value = "/searching" )
public class SearchingController {

    @Autowired
    SearchingService searchingService;

    @Autowired
    FileService fileService;

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ResultData>> search(@RequestBody ArrayList<SearchQuery> searchQueries) throws ParseException {

        if (searchQueries.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("SEARCH: " + searchQueries.toString());

        List<QueryBuilder> queries = new ArrayList<>();
        List<ResultHighlighter> resultHighlighters = new ArrayList<>();

        BoolQueryBuilder builder = QueryBuilders.boolQuery();

        for (SearchQuery query: searchQueries){
            QueryBuilder queryBuilder = null;
            if (query.getType().equals("regular")){
                queryBuilder = QueryCreator.buildQuery(QueryType.REGULAR, query.getField(), query.getValue());
            } else if (query.getType().equals("phrase")){
                queryBuilder = QueryCreator.buildQuery(QueryType.PHRASE, query.getField(), query.getValue());
            }
            if (queryBuilder != null) {
                queries.add(queryBuilder);

                if (query.getOperator().equals("AND")){
                    builder.must(queryBuilder);
                } else if (query.getOperator().equals("OR")){
                    builder.should(queryBuilder);
                }
            }
            resultHighlighters.add(new ResultHighlighter(query.getField(), query.getValue()));
        }

        List<ResultData> results = new ArrayList<>();

        if (queries.size() > 1){
            results = searchingService.getResultData(builder, resultHighlighters);
        } else if (queries.size() == 1) {
            results = searchingService.getResultData(queries.get(0), resultHighlighters);
        }

        System.out.println("Results found: " + results.size());

        if (results.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);

    }



    @RequestMapping(
            value = "/geo-distance",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ReviewerResult>> searchGeoDistance(@RequestBody GeoDistanceQuery geoDistanceQuery) throws ParseException {

        System.out.println("Entered geo distance search endpoint");

        List<ReviewerResult> results;

        String distance = geoDistanceQuery.getDistance().toString() + "km";

        BoolQueryBuilder builder = QueryBuilders.boolQuery();

        if (geoDistanceQuery.getAuthors().isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Author> authors = new ArrayList<>();

        for (AuthorDTO dto: geoDistanceQuery.getAuthors()){
            authors.add(new Author(dto));
        }

        for (Author author: authors){

            GeoPoint geoPoint = Indexer.getInstance().getHandler().getCoordinates(author.getCity(), author.getState());

            QueryBuilder queryBuilder = new GeoDistanceQueryBuilder("coordinates")
                    .point(geoPoint.getLat(), geoPoint.getLon())
                    .distance(distance);

            builder.mustNot(queryBuilder);
        }


        results = searchingService.getReviewerResultData(builder);

        System.out.println("Results found: " + results.size());

        if (results.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(results, HttpStatus.OK);

    }

    @RequestMapping(
            value = "/more-like-this",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ReviewerResult>> searchMoreLikeThis(@RequestParam("file") MultipartFile file) throws IOException {

        System.out.println("Entered more like this search endpoint");

        List<ReviewerResult> results;

        String[] fields = new String[1];
        fields[0] = "content";

        MoreLikeThisQueryBuilder.Item[] items = new MoreLikeThisQueryBuilder.Item[0];

        String content = getContent(file);

        String[] likeText = new String[1];

        likeText[0] = content;

        MoreLikeThisQueryBuilder queryBuilder = new MoreLikeThisQueryBuilder(fields, likeText, items);

        queryBuilder.stopWords(getStopWords());
        queryBuilder.analyzer("serbian_analyzer");

        queryBuilder.minDocFreq(1);
        queryBuilder.minTermFreq(5);
        queryBuilder.maxQueryTerms(50);

        queryBuilder.minimumShouldMatch("30%");
        results = searchingService.getReviewerResultDataMoreLikeThis(queryBuilder);

        System.out.println("Results found: " + results.size());

        if (results.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(results, HttpStatus.OK);
    }


    private List<ResultData> helperFunction(List<QueryBuilder> queries, List<ResultHighlighter> resultHighlighters) {
        List<ResultData> results;

        if (queries.size() > 1){

            BoolQueryBuilder builder = QueryBuilders.boolQuery();

            for (QueryBuilder query: queries){
                builder.must(query);
            }

            results = searchingService.getResultData(builder, resultHighlighters);
        } else {
            results = searchingService.getResultData(queries.get(0), resultHighlighters);
        }

        return results;
    }

    public String[] getStopWords(){
        final String[] SERBIAN_STOP_WORDS = {
                "biti", "ne",
                "jesam", "sam", "jesi", "si", "je", "jesmo", "smo", "jeste", "ste", "jesu", "su",
                "nijesam", "nisam", "nijesi", "nisi", "nije", "nijesmo", "nismo", "nijeste", "niste", "nijesu", "nisu",
                "budem", "budeš", "bude", "budemo", "budete", "budu",
                "budes",
                "bih", "bi", "bismo", "biste", "biše",
                "bise",
                "bio", "bili", "budimo", "budite", "bila", "bilo", "bile",
                "ću", "ćeš", "će", "ćemo", "ćete",
                "neću", "nećeš", "neće", "nećemo", "nećete",
                "cu", "ces", "ce", "cemo", "cete",
                "necu", "neces", "nece", "necemo", "necete",
                "mogu", "možeš", "može", "možemo", "možete",
                "mozes", "moze", "mozemo", "mozete"};

        return SERBIAN_STOP_WORDS;
    }

    public String getContent(MultipartFile file) throws IOException {
        String fileName = fileService.saveFile(file);
        RandomAccessFile randomAccessFile = new RandomAccessFile(new File(fileName), "r");
        PDFParser parser = new PDFParser(randomAccessFile);
        parser.parse();
        String content = Indexer.getInstance().getHandler().getText(parser);
        randomAccessFile.close();

        return content;
    }
}


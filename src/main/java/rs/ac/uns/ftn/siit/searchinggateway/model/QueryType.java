package rs.ac.uns.ftn.siit.searchinggateway.model;

public enum QueryType {
	
		REGULAR,
		PHRASE
}

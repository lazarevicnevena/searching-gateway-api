package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IndexConfiguration {

    private Object settings;
    private Object mappings;
}

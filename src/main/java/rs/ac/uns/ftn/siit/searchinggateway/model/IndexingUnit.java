package rs.ac.uns.ftn.siit.searchinggateway.model;

import io.searchbox.annotations.JestId;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IndexingUnit {

    private String title;
    private String journal;
    private String subjectArea;
    private String doi;
    private List<String> keywords = new ArrayList<>();
    private String abstract_;
    private Boolean openAccess;
    private List<Author> authors = new ArrayList<>();
    private String content;
    private List<Reviewer> reviewers = new ArrayList<>();

    @JestId
    private String filename;
    private String filedate;

}

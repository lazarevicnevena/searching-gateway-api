package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UploadModel {

    private MetaDataInfo metaDataInfo;

    private MultipartFile file;

    private List<Reviewer> reviewers = new ArrayList<>();
}

package rs.ac.uns.ftn.siit.searchinggateway.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AuthorDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;

}

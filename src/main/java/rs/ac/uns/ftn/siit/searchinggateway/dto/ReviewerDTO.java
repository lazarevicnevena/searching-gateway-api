package rs.ac.uns.ftn.siit.searchinggateway.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReviewerDTO {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;
    private String rank;

}


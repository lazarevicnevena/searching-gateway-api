package rs.ac.uns.ftn.siit.searchinggateway.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import io.searchbox.client.JestResult;
import lombok.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import rs.ac.uns.ftn.siit.searchinggateway.model.*;
import org.elasticsearch.index.query.QueryBuilder;


@Getter
@Setter
@ToString
public class ResultRetriever {
	
	private static int maxHits = 10;
	
	private static JestClient client;

	private static String ELASTIC_SEARCH_ADDRESS;

	static {
		ResourceBundle rb=ResourceBundle.getBundle("application");
		String address =rb.getString("esAddress");
		Integer port = Integer.parseInt(rb.getString("esPort"));
		ELASTIC_SEARCH_ADDRESS = address + ":" + port.toString();
	}
	
	static {
		JestClientFactory factory = new JestClientFactory();
		 factory.setHttpClientConfig(new HttpClientConfig
		                        .Builder(ELASTIC_SEARCH_ADDRESS)
		                        .multiThreaded(true)
		                        .build());
		ResultRetriever.client = factory.getObject();
		
	}

	public static List<ResultData> getResults(QueryBuilder query,
                                              List<ResultHighlighter> requiredHighlights) {
		if (query == null) {
			return null;
		}
			
		List<ResultData> results = new ArrayList<>();

		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.query(query);
		searchSourceBuilder.size(maxHits);	
		
		HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("title");
        highlightBuilder.field("journal");
        highlightBuilder.field("subjectArea");
        highlightBuilder.field("content");
        highlightBuilder.field("keywords");
        highlightBuilder.field("authors.firstName");
        highlightBuilder.field("authors.lastName");
        highlightBuilder.preTags("<span style=\"color:red;\">").postTags("</span>");
        highlightBuilder.fragmentSize(200);
        searchSourceBuilder.highlighter(highlightBuilder);
		
		Search search = new Search.Builder(searchSourceBuilder.toString())
                // multiple index or types can be added.
                .addIndex("digitallibrary")
                .addType("book")
                .build();
		
		SearchResult result;
		try {
			result = client.execute(search);
			List<SearchResult.Hit<IndexingUnit, Void>> hits = result.getHits(IndexingUnit.class);
			
			ResultData rd;
			
			for (SearchResult.Hit<IndexingUnit, Void> sd : hits) {
				String highlight = "";
				for (String hf : sd.highlight.keySet() ) {
					for (ResultHighlighter rh : requiredHighlights) {
						if(hf.equals(rh.getField())){
							if (!highlight.contains(sd.highlight.get(hf).toString())){
                                highlight += sd.highlight.get(hf).toString();
                            }
						}
					}
				}
				String authors = "";
				for (Author a: sd.source.getAuthors()){
				    authors += a.getFirstName() + " " + a.getLastName() + " [ " + a.getCity() + ", " + a.getState() + " ], ";
                }
                authors = authors.substring(0, authors.length()-2);
				rd = new ResultData(
				        sd.source.getTitle(),
                        sd.source.getJournal(),
                        sd.source.getSubjectArea(),
                        sd.source.getKeywords().toString(),
                        sd.source.getFilename(),
						sd.source.getOpenAccess(),
						sd.source.getDoi(),
						authors,
						highlight);

				results.add(rd);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public static List<ReviewerResult> getReviewerResultData(QueryBuilder query) {
		if (query == null) {
			return null;
		}

		List<ReviewerResult> results = new ArrayList<>();

		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.query(query);
		searchSourceBuilder.size(50);

		Search search = new Search.Builder(searchSourceBuilder.toString())
				// multiple index or types can be added.
				.addIndex("reviewers")
				.addType("reviewer")
				.build();

		SearchResult result;
		try {
			result = client.execute(search);
			List<SearchResult.Hit<Reviewer, Void>> hits = result.getHits(Reviewer.class);

			ResultData rd;

			for (SearchResult.Hit<Reviewer, Void> sd : hits) {

				ReviewerResult reviewerResult = new ReviewerResult(
						sd.source.getUsername(),
						sd.source.getFirstName(),
						sd.source.getLastName(),
						sd.source.getEmail(),
						sd.source.getCity(),
						sd.source.getState(),
						sd.source.getRank());

				results.add(reviewerResult);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return results;
	}

    public static List<ReviewerResult> getReviewerResultDataMoreLikeThis(QueryBuilder query) {
        if (query == null) {
            return null;
        }

        List<ReviewerResult> results = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        searchSourceBuilder.size(5);

        Search search = new Search.Builder(searchSourceBuilder.toString())
                // multiple index or types can be added.
                .addIndex("digitallibrary")
                .addType("book")
                .build();

        SearchResult result;
        try {
            result = client.execute(search);
            List<SearchResult.Hit<IndexingUnit, Void>> hits = result.getHits(IndexingUnit.class);

            for (SearchResult.Hit<IndexingUnit, Void> sd : hits) {
				if (sd.score >= 14){
                    for (Reviewer r: sd.source.getReviewers()){
                        ReviewerResult reviewerResult = new ReviewerResult(
                                r.getUsername(),
                                r.getFirstName(),
                                r.getLastName(),
                                r.getEmail(),
                                r.getCity(),
                                r.getState(),
                                r.getRank());

                        boolean flag = true;

                        if (!results.isEmpty()){
                            for (ReviewerResult result1: results){
                                if (result1.getEmail().equals(reviewerResult.getEmail())){
                                    flag = false;
                                    break;
                                }
                            }
                        }

                        if (flag){
                            results.add(reviewerResult);
                        }
                    }
				}
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return results;
    }


}

package rs.ac.uns.ftn.siit.searchinggateway.model;

import lombok.*;
import rs.ac.uns.ftn.siit.searchinggateway.dto.AuthorDTO;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GeoDistanceQuery {

    private Integer distance;

    private List<AuthorDTO> authors = new ArrayList<>();


}

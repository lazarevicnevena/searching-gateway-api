package rs.ac.uns.ftn.siit.searchinggateway.indexing;

import com.google.gson.Gson;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Get;
import io.searchbox.core.Index;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.DeleteIndex;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONObject;
import rs.ac.uns.ftn.siit.searchinggateway.handler.PDFHandler;
import rs.ac.uns.ftn.siit.searchinggateway.model.IndexingUnit;
import rs.ac.uns.ftn.siit.searchinggateway.model.Reviewer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class Indexer {

    private static String ELASTIC_SEARCH_ADDRESS;
    private static Integer ELASTIC_SEARCH_PORT;

    static {
        ResourceBundle rb=ResourceBundle.getBundle("application");
        ELASTIC_SEARCH_ADDRESS=rb.getString("esAddress");
        ELASTIC_SEARCH_PORT = Integer.parseInt(rb.getString("esPort"));
    }

    private JestClient client;

    private static Indexer indexer = new Indexer();

    public static Indexer getInstance(){
        return indexer;
    }

    private Indexer(String address, int port) {
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder(address + ":" + port)
                .multiThreaded(true)
                .maxConnectionIdleTime(3000L, TimeUnit.MILLISECONDS)
                .readTimeout(60000*10)
                .build());
        client = factory.getObject();
    }

    public PDFHandler getHandler(){
        return new PDFHandler();
    }

    public boolean addIndexingUnit(IndexingUnit unit) throws UnsupportedEncodingException{
        IndexingUnit u = unit;
        u.setFilename(URLEncoder.encode(unit.getFilename(), "UTF-8"));
        Index index = new Index.Builder(u).index("digitallibrary").type("book").build();
        JestResult result;
        try {
            result = client.execute(index);
            return result.isSucceeded();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    public boolean addReviewerIndexingUnit(Reviewer reviewer) throws IOException {

        Get get = new Get.Builder("reviewers", reviewer.getEmail()).type("reviewer").build();

        JestResult reviewerExists = client.execute(get);

        if (reviewerExists.isSucceeded()){
            System.out.println("Reviewer already exists!");
            return false;
        }

        reviewer.setCoordinates(Indexer.getInstance().getHandler().getCoordinates(reviewer.getCity(), reviewer.getState()));
        Index index = new Index.Builder(reviewer).index("reviewers").type("reviewer").build();
        JestResult result;
        try {
            result = client.execute(index);
            return result.isSucceeded();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }


    public boolean setScientificCenterAnalyzer(String analyzer, String mappings) throws IOException {

        DeleteIndex delete = new DeleteIndex.Builder("digitallibrary").build();

        JestResult r1 = client.execute(delete);

        System.out.println("Index deleted: " + r1.isSucceeded());

        System.out.println("Analyzer: " + analyzer);

        try{

            CreateIndex createIndex = new CreateIndex.Builder("digitallibrary").settings(analyzer).mappings(mappings).build();

            JestResult result = client.execute(createIndex);

            System.out.println("Result: " + result.isSucceeded());

            return result.isSucceeded();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean setReviewersAnalyzer(String analyzer, String mappings) throws IOException {


        System.out.println("Analyzer: " + analyzer);

        System.out.println("Mappings: " + mappings);

        try{

            CreateIndex createIndex = new CreateIndex.Builder("reviewers").settings(analyzer).mappings(mappings).build();

            JestResult result = client.execute(createIndex);

            System.out.println("Result: " + result.isSucceeded());

            return result.isSucceeded();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }


    private Indexer() {
        this(ELASTIC_SEARCH_ADDRESS, ELASTIC_SEARCH_PORT);
    }

}
